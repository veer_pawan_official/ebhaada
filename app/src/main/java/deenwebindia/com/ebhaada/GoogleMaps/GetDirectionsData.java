package deenwebindia.com.ebhaada.GoogleMaps;

import android.os.AsyncTask;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.HashMap;

public class GetDirectionsData extends AsyncTask<Object, String, String> {
    GoogleMap mMap;
    String url;
    String googleDirectionData;
    String duration, distance;
    LatLng latLng;

    @Override
    protected String doInBackground(Object... objects) {
        mMap = (GoogleMap) objects[0];
        url = (String) objects[1];
        latLng = (LatLng) objects[2];
        DownloadUrl downloadUrl = new DownloadUrl();
        try {
            googleDirectionData = downloadUrl.readUrl(url);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return googleDirectionData;
    }

    @Override
    protected void onPostExecute(String s) {
//        String[] directionsList;
//        directionsList = null;
//        directionsList = parser.parseDirections(s);
//        displayDirection(directionsList);
        HashMap<String, String> directionList = null;
        DataParser parser = new DataParser();
        directionList = parser.parseDirections(s);
        duration= directionList.get("duration");
        distance= directionList.get("distance");

        mMap.clear();
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.draggable(true);
        markerOptions.title("Duration=" + duration);
        markerOptions.snippet("Distance=" + distance);
        mMap.addMarker(markerOptions);

        // }
//        List<HashMap<String, String>> nearbyPlaceList = null;
//        DataParser parser = new DataParser();
//        nearbyPlaceList = parser.parse(s);
//        showNearbyPlaces(nearbyPlaceList);
    }

//    public void displayDirection(String[] directionList) {
//        int count = directionList.length;
//        for (int i = 0; i < count; i++) {
//            PolylineOptions options = new PolylineOptions();
//            options.color(Color.RED);
//            options.width(10);
//            options.addAll(PolyUtil.decode(directionList[i]));
//
//            mMap.addPolyline(options);
//        }
}
