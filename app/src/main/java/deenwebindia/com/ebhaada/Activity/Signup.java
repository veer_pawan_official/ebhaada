package deenwebindia.com.ebhaada.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import deenwebindia.com.ebhaada.R;

public class Signup extends AppCompatActivity {

    Button btnLogin ;
    EditText inputEmail, inputPassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        inputEmail = (EditText) findViewById(R.id.name);
        inputPassword = (EditText) findViewById(R.id.email);
        btnLogin = (Button) findViewById(R.id.btn_login);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Signup.this, OTP.class);
                startActivity(intent);
            }
        });


    }
}
