package deenwebindia.com.ebhaada.Activity.OTPapi;

import com.google.gson.annotations.SerializedName;

public class UserInfoData {


    @SerializedName("Userid")
    private int Userid;
    @SerializedName("UserType")
    private int UserType;

    @SerializedName("FullName")
    private String FullName;

    @SerializedName("EmailAddress")
    private String EmailAddress;

    @SerializedName("MobileNo")
    private String MobileNo;


    public int getUserid() {
        return Userid;
    }

    public void setUserid(int userid) {
        Userid = userid;
    }

    public int getUserType() {
        return UserType;
    }

    public void setUserType(int userType) {
        UserType = userType;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public String getEmailAddress() {
        return EmailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        EmailAddress = emailAddress;
    }

    public String getMobileNo() {
        return MobileNo;
    }

    public void setMobileNo(String mobileNo) {
        MobileNo = mobileNo;
    }
}
