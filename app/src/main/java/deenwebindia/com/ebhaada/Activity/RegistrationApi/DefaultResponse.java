package deenwebindia.com.ebhaada.Activity.RegistrationApi;

import com.google.gson.annotations.SerializedName;

public class DefaultResponse {

    @SerializedName("success")
    private int succs;

    @SerializedName("msg")
    private String massages;

    @SerializedName("VerifyCode")
    private int verifys;

    public DefaultResponse(int succ, String massage, int verify) {
        this.succs = succ;
        this.massages = massage;
        this.verifys = verify;
    }

    public int getSucc() {
        return succs;
    }

    public void setSucc(int succ) {
        this.succs = succ;
    }

    public String getMassage() {
        return massages;
    }

    public void setMassage(String massage) {
        this.massages = massage;
    }

    public int getVerify() {
        return verifys;
    }

    public void setVerify(int verify) {
        this.verifys = verify;
    }
}
