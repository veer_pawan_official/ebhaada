package deenwebindia.com.ebhaada.Activity.OTPapi;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import deenwebindia.com.ebhaada.Activity.OTPapi.UserInfoData;

public class OTPResponse {
    @SerializedName("success")
    private int success;

    @SerializedName("msg")
    private String msg;


    @SerializedName("UserInfoData")
    private List<UserInfoData> UserInfoData;


    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<deenwebindia.com.ebhaada.Activity.OTPapi.UserInfoData> getUserInfoData() {
        return UserInfoData;
    }

    public void setUserInfoData(List<deenwebindia.com.ebhaada.Activity.OTPapi.UserInfoData> userInfoData) {
        UserInfoData = userInfoData;
    }
}
