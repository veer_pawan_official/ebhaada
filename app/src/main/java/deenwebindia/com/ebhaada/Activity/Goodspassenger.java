package deenwebindia.com.ebhaada.Activity;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.widget.RadioButton;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import deenwebindia.com.ebhaada.R;


public class Goodspassenger extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private static final int ERROR_DIALOG_REQUEST = 9001;

    RadioButton truck;
    RadioButton car;
//    DrawerLayout drawerLayout;
//    Toolbar toolbar;
//    ActionBarDrawerToggle actionBarDrawerToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goodpassengers);
        if (isServicesOK()) {
            init();
        }
    }

    private void init() {
        car = (RadioButton) findViewById(R.id.Car);
        car.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Goodspassenger.this, Getlocation.class);
                startActivity(intent);
            }
        });
        truck = (RadioButton) findViewById(R.id.Truck);
        truck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Goodspassenger.this, Getlocation.class);
                startActivity(intent);
            }
        });
//        setUpToolbar();
    }


//    private void setUpToolbar() {
//        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
//        toolbar = (Toolbar) findViewById(R.id.Toolbar);
//        setSupportActionBar(toolbar);
//        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, 0, 0);
//        drawerLayout.addDrawerListener(actionBarDrawerToggle);
//        getSupportActionBar().setDisplayShowTitleEnabled(false);
//        actionBarDrawerToggle.syncState();
//    }

    public boolean isServicesOK() {
        Log.d(TAG, "isServicesOK: checking google services version");

        int available = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(Goodspassenger.this);

        if (available == ConnectionResult.SUCCESS) {
            //everything is fine and the user can make map requests
            Log.d(TAG, "isServicesOK: Google Play Services is working");
            return true;
        } else if (GoogleApiAvailability.getInstance().isUserResolvableError(available)) {
            //an error occured but we can resolve it
            Log.d(TAG, "isServicesOK: an error occured but we can fix it");
            Dialog dialog = GoogleApiAvailability.getInstance().getErrorDialog(Goodspassenger.this, available, ERROR_DIALOG_REQUEST);
            dialog.show();
        } else {
            Toast.makeText(this, "You can't make map requests", Toast.LENGTH_SHORT).show();
        }
        return false;
    }
}