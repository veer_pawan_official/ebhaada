package deenwebindia.com.ebhaada.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import deenwebindia.com.ebhaada.Activity.RegistrationApi.DefaultResponse;
import deenwebindia.com.ebhaada.Activity.RegistrationApi.RetrofitRegistration;
import deenwebindia.com.ebhaada.R;
import deenwebindia.com.ebhaada.utils.DialogsUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Registration extends AppCompatActivity implements View.OnClickListener {
    DefaultResponse defaultResponse;
    EditText Email, Password, Mobile, Name, CPassword;
    ProgressDialog progressDialog;

     String emails;
     String passwords;
     String mobiles;
     String names,confirmpasswords;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        Name = findViewById(R.id.name);
        Email = findViewById(R.id.email);
        Password = findViewById(R.id.password);
        Mobile = findViewById(R.id.mobile);
        CPassword = findViewById(R.id.confirmpassword);
        findViewById(R.id.allready).setOnClickListener(this);
        findViewById(R.id.sign).setOnClickListener(this);
    }

    void userSignUp() {
        emails = Email.getText().toString().trim();
        passwords = Password.getText().toString().trim();
        mobiles = Mobile.getText().toString().trim();
        names = Name.getText().toString().trim();
        confirmpasswords = CPassword.getText().toString().trim();
        if (emails.isEmpty()) {
            Email.setError("Email is required");
            Email.requestFocus();
            return;
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(emails).matches()) {
            Email.setError("Enter a valid email");
            Email.requestFocus();
            return;
        }
        if (passwords.isEmpty()) {
            Password.setError("Password required");
            Password.requestFocus();
            return;
        }
        if (passwords.length() < 6) {
            Password.setError("Password should be 6 char long");
            Password.requestFocus();
            return;
        }
        if (confirmpasswords.length() < 6) {
            CPassword.setError("Password should be 6 char long");
            CPassword.requestFocus();
            return;
        }
        if (names.isEmpty()) {
            Name.setError("Name Required");
            Name.requestFocus();
            return;
        }
        if (mobiles.length() < 10) {
            Mobile.setError("Enter a valid Mobile Number");
            Mobile.requestFocus();
        }else{
            usersignup();
        }

    }

    private void usersignup() {

        progressDialog= DialogsUtils.showProgressDialog(this,"Loading Please wait!!");

        Call<DefaultResponse> call = RetrofitRegistration
                .getInstance()
                .getApiR()
                .createUser(names, mobiles, emails, passwords, confirmpasswords);

        Log.e("INFOO", names + "    " + emails + "   " + passwords + "   " + confirmpasswords + "     " + mobiles);
        call.enqueue(new Callback<DefaultResponse>() {
            @Override
            public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                Toast.makeText(Registration.this, response.code() + "", Toast.LENGTH_LONG).show();
                progressDialog.dismiss();
                if (response.code() == 200) {
                    defaultResponse = response.body();
                    Log.e("OTPNUMBER",defaultResponse.getVerify()+"");
                    Toast.makeText(Registration.this, defaultResponse.getMassage(), Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(Registration.this, dialog.class);
                    intent.putExtra("otp_number", defaultResponse.getVerify()+"");
                    intent.putExtra("mobile_number", mobiles);
                    intent.putExtra("name", names);
                    intent.putExtra("password", passwords);
                    intent.putExtra("confirmpassword", confirmpasswords);
                    intent.putExtra("email", emails);

                    startActivity(intent);
                } else if (response.code() == 422) {

                    Toast.makeText(Registration.this, "User already exist", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<DefaultResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(Registration.this, t + "", Toast.LENGTH_LONG).show();
            }
        });

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sign:
                userSignUp();
                break;
            case R.id.allready:
                Intent intent = new Intent(Registration.this, Login.class);
                startActivity(intent);
                break;
        }
    }
}
