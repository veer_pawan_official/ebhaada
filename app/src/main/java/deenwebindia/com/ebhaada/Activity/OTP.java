package deenwebindia.com.ebhaada.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import deenwebindia.com.ebhaada.Activity.OTPapi.OTPResponse;
import deenwebindia.com.ebhaada.Activity.OTPapi.UserInfoData;
import deenwebindia.com.ebhaada.MobileApi.RetrofitMobile;
import deenwebindia.com.ebhaada.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class OTP extends AppCompatActivity implements View.OnClickListener {
    Button verify;
    //Pinview pinview;
    EditText editText;
    String user_otp;
    String user_mobile;
    String verify_otp;
    OTPResponse otpResponse;
    String textView1;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
//      editText = findViewById(R.id.mypinview);
        editText = findViewById(R.id.et_otp);
        verify = (Button) findViewById(R.id.buttonSignIn);
        findViewById(R.id.buttonSignIn).setOnClickListener(this);
        Intent i = getIntent();
        user_otp = i.getStringExtra("otp_number");
        user_mobile = i.getStringExtra("mobile_number");
        //textView=i.getStringExtra("number");
        Intent in = getIntent();
        String tv1= in.getExtras().getString("location");
//        textView1.split(tv1);
//        verify.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(OTP.this, Congratulation.class);
//                startActivity(intent);
//            }
//        });
//
//        pinview = (Pinview) findViewById(R.id.mypinview);
//
//        pinview.setPinViewEventListener(new Pinview.PinViewEventListener() {
//            @Override
//            public void onDataEntered(Pinview pinview, boolean b) {
//
//            }
        //   });


    }

    void userslogin() {

        verify_otp = editText.getText().toString().trim();

        Call<OTPResponse> call = RetrofitMobile
                .getInstance()
                .getApi()
                .userotp(user_mobile, user_otp, verify_otp);
        Log.e("vall", user_mobile + "    " + user_otp + "   " + verify_otp);

        call.enqueue(new Callback<OTPResponse>() {
            @Override
            public void onResponse(Call<OTPResponse> call, Response<OTPResponse> response) {
                if (response.code() == 200) {
                    otpResponse = response.body();
                    List<UserInfoData> userInfoData1 = new ArrayList<>();
                    List<UserInfoData> userInfoData = response.body().getUserInfoData();
                   // Toast.makeText(OTP.this, otpResponse.getUserInfoData().get(0).getFullName(), Toast.LENGTH_LONG).show();
                   //  userInfoData.get(0);
                    Intent i = new Intent(getApplicationContext(), Goodspassenger.class);
                    startActivity(i);
                } else {
                    Toast.makeText(OTP.this, otpResponse.getMsg(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<OTPResponse> call, Throwable t) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonSignIn:
                userslogin();
                break;
        }
    }
}


