package deenwebindia.com.ebhaada.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;


import deenwebindia.com.ebhaada.MobileApi.RetrofitMobile;
import deenwebindia.com.ebhaada.MobileApi.defaultResponsem;
import deenwebindia.com.ebhaada.R;

import deenwebindia.com.ebhaada.utils.DialogsUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Login extends AppCompatActivity implements View.OnClickListener {

    Button btn;
    defaultResponsem dr;
    String mobile;
    private EditText editText;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        editText = findViewById(R.id.enternumber);
        btn = (Button) findViewById(R.id.Next);

        findViewById(R.id.Next).setOnClickListener(this);
        findViewById(R.id.Newuser).setOnClickListener(this);
    }

    void userSignUp() {
        mobile = editText.getText().toString().trim();

        if (mobile.length() < 10) {
            editText.setError("Enter the correct number");
            editText.requestFocus();
            return;
        }
        else{

            usersignup();
        }

    }

    private void usersignup() {

        progressDialog= DialogsUtils.showProgressDialog(this,"Please wait");

        Call<defaultResponsem> call = RetrofitMobile
                .getInstance()
                .getApi()
                .createmobile(mobile);
        call.enqueue(new Callback<defaultResponsem>() {
            @Override
            public void onResponse(Call<defaultResponsem> call, Response<defaultResponsem> response) {

                progressDialog.dismiss();
                if (response.code() == 200) {
                    dr = response.body();
                    Log.e("OTPNUMBER",dr.getVerify()+"");
                    Toast.makeText(Login.this, dr.getMassage(), Toast.LENGTH_LONG).show();

                    Intent intent = new Intent(Login.this, OTP.class);
                    intent.putExtra("otp_number", dr.getVerify()+"");
                    intent.putExtra("mobile_number", mobile);
                    intent.putExtra("number",mobile.split(" ").toString());
                    startActivity(intent);
                }   else if (response.code() == 422) {
                    Toast.makeText(Login.this, "User already exist", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<defaultResponsem> call, Throwable t) {
                progressDialog.dismiss();

            }
        });
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.Next:
                userSignUp();

                break;
            case R.id.Newuser:
                Intent intent1 = new Intent(Login.this, Registration.class);
                startActivity(intent1);
                break;
        }
    }
}
