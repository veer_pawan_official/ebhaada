package deenwebindia.com.ebhaada.Activity.RegistrationApi;

import deenwebindia.com.ebhaada.MobileApi.mobileapi;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitRegistration {
    private static final String BASE_URL = "http://www.ebhada.com/api/";
    private static RetrofitRegistration mInstance;
    private Retrofit retrofitR;

    private RetrofitRegistration(){
        retrofitR = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
    public static synchronized RetrofitRegistration getInstance(){
        if(mInstance==null){
            mInstance= new RetrofitRegistration();
        }
        return mInstance;
    }
    public mobileapi getApiR()
    {
        return retrofitR.create(mobileapi.class);
    }


}
