package deenwebindia.com.ebhaada.Activity.Registerverify;

import com.google.gson.annotations.SerializedName;

public class DefaultResponseReg {

    @SerializedName("success")
    private int success;

    @SerializedName("msg")
    private String msg;

    public DefaultResponseReg(int success, String msg) {
        this.success = success;
        this.msg = msg;
    }

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}

