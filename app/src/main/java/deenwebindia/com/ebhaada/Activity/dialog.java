package deenwebindia.com.ebhaada.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import deenwebindia.com.ebhaada.Activity.Registerverify.DefaultResponseReg;
import deenwebindia.com.ebhaada.MobileApi.RetrofitMobile;
import deenwebindia.com.ebhaada.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class dialog extends AppCompatActivity implements View.OnClickListener {

    Button verifyb;
    EditText editText;
    String user_otp;
    String user_mobile;
    String verify_otp;
    String name_verify;
    String password_verify;
    String confipassword_verify;
    String email_verify;
    DefaultResponseReg defaultResponseReg;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog);
        editText = findViewById(R.id.otp);
        verifyb = (Button) findViewById(R.id.otp_submit);
        findViewById(R.id.otp_submit).setOnClickListener(this);
        Intent i = getIntent();
        user_otp = i.getStringExtra("otp_number");
        user_mobile = i.getStringExtra("mobile_number");
        name_verify = i.getStringExtra("name");
        password_verify = i.getStringExtra("password");
        confipassword_verify = i.getStringExtra("confirmpassword");
        email_verify = i.getStringExtra("email");
    }

    void usersverify() {
        verify_otp = editText.getText().toString().trim();

        Call<DefaultResponseReg> call = RetrofitMobile
                .getInstance()
                .getApi()
                .registerverify(name_verify, user_mobile, email_verify, password_verify, confipassword_verify,
                        user_otp, verify_otp);

        Log.e("users", name_verify + "   " + user_mobile + "    " + email_verify + "   " + password_verify +
                "    " + confipassword_verify + "    " + user_otp + "   " + verify_otp + "   ");
        call.enqueue(new Callback<DefaultResponseReg>() {
            @Override
            public void onResponse(Call<DefaultResponseReg> call, Response<DefaultResponseReg> response) {
                if (response.code() == 200) {

                    defaultResponseReg = response.body();

                    Toast.makeText(dialog.this, defaultResponseReg.getMsg(), Toast.LENGTH_LONG).show();
                    Intent i = new Intent(getApplicationContext(), Congratulation.class);
                    startActivity(i);

                } else {
                    Toast.makeText(dialog.this, defaultResponseReg.getMsg(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<DefaultResponseReg> call, Throwable t) {

            }
        });

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.otp_submit:
                usersverify();
                break;
        }
    }
}
