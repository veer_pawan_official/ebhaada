package deenwebindia.com.ebhaada.MobileApi;

import deenwebindia.com.ebhaada.Activity.Registerverify.DefaultResponseReg;
import deenwebindia.com.ebhaada.Activity.OTPapi.OTPResponse;
import deenwebindia.com.ebhaada.Activity.RegistrationApi.DefaultResponse;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface mobileapi {
    @FormUrlEncoded
    @POST("login-mobile")
    Call<defaultResponsem> createmobile(
            @Field("MobileNo") String mobile
    );

    @FormUrlEncoded
    @POST("login-verify")
    Call<OTPResponse> userotp(
            @Field("MobileNo") String user_mobile,
            @Field("OTPNumber") String user_otp,
            @Field("OTPVerify") String verify_otp
    );

    @FormUrlEncoded
    @POST("register")
    Call<DefaultResponse> createUser(
            @Field(" FullName") String name,
            @Field("MobileNo") String mobile,
            @Field("EmailAddress") String email,
            @Field("Password") String password,
            @Field("CPassword") String confirmpassword
    );

    @FormUrlEncoded
    @POST("register-verify")
    Call<DefaultResponseReg> registerverify(
            @Field("FullName") String name_verify,
            @Field("MobileNo") String user_mobile,
            @Field("EmailAddress") String email_verify,
            @Field("Password") String password_verify,
            @Field("CPassword") String confipassword_verify,
            @Field("OTPNumber") String user_otp,
            @Field("OTPVerify") String verify_otp
    );

}
