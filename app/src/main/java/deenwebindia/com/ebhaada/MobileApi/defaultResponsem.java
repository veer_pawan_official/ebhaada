package deenwebindia.com.ebhaada.MobileApi;

import com.google.gson.annotations.SerializedName;

public class defaultResponsem {
  @SerializedName("success")
    private int succ;
  @SerializedName("msg")
    private String massage;
  @SerializedName("VerifyCode")
    private String verify;

    public defaultResponsem(int succ, String massage, String verify) {
        this.succ = succ;
        this.massage = massage;
        this.verify = verify;
    }

    public int getSucc() {
        return succ;
    }

    public void setSucc(int succ) {
        this.succ = succ;
    }

    public String getMassage() {
        return massage;
    }

    public void setMassage(String massage) {
        this.massage = massage;
    }

    public String getVerify() {
        return verify;
    }

    public void setVerify(String verify) {
        this.verify = verify;
    }
}
