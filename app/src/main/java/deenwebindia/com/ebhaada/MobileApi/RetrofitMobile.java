package deenwebindia.com.ebhaada.MobileApi;

import deenwebindia.com.ebhaada.MobileApi.RetrofitMobile;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitMobile{
        private static final String BASE_URL = "http://www.ebhada.com/api/";
        private static RetrofitMobile mInstance;
        private Retrofit retrofitm;

        private RetrofitMobile() {
            retrofitm = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

       public static synchronized RetrofitMobile getInstance(){
            if (mInstance== null){
                mInstance= new RetrofitMobile();
            }
            return mInstance;
       }
        public mobileapi getApi(){
            return retrofitm.create(mobileapi.class);
        }
}

